import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardHttpService {
  private baseUrl = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  getMessage(): Observable<any> {
    console.log("wszełem");
    return this.http.get(`${this.baseUrl}`);
  }
}
