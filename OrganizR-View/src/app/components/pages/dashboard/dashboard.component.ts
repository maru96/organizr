import { Component, OnInit } from '@angular/core';
import {DashboardHttpService} from "../../../services/dashboard-http-service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public message$: Observable<string>;

  constructor(private dashboardHttpSevice: DashboardHttpService) { }

  ngOnInit(): void {
    this.message$ = this.dashboardHttpSevice.getMessage();
  }

}
