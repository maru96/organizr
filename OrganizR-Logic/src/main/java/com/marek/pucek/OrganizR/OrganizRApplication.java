package com.marek.pucek.OrganizR;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganizRApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrganizRApplication.class, args);
	}

}
